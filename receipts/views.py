from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create your views here.
def redirect_to_receipt_list(request):
    return redirect("home")


@login_required
def all_the_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "all_receipts": receipts,
    }
    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    cat_view = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": cat_view,
    }
    return render(request, "receipts/category.html", context)


@login_required
def account_list(request):
    acct_view = Account.objects.filter(owner=request.user)
    context = {
        "accounts": acct_view,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/catcreate.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/acctcreate.html", context)
